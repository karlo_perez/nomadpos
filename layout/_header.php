<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-compatible" content="IE-Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">
    <!-- <meta name="robots" content="noarchive, nosnippet, noimageindex"> -->
    
    <title>NomadPOS</title>
    
    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/png" href="contents/favicon_nomadpos blue.png" media="screen">

    <!-- Bootsrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Font-awesome 4 -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
    <!-- Main Styles -->
    <link rel="stylesheet" href="styles.css" media="screen">

    <!-- <link rel="manifest" href="components/_manifest.webmanifest"> -->
</head>
<body>

    <?php require_once '_navigation.php'; ?>