<div class="menu">
    <a class="nav-mobile" href="javascript:void(0)" onclick="mobileMenuToggle()">
        <i class="fas fa-bars"></i>
        <i class="fas fa-times"></i>
    </a>

    <div class="mobile-logo-container">
        <a href="index.php">
            <img src="contents/logo-only_nomadpos blue (white).png" alt="">
        </a>
    </div>

    <ul class="nav" id="nav">
        <li class="logo-container"><a href="#">
            <img src="contents/logo-retina_nomadpos blue.png" alt="">
        </a></li>
        <li><a href="index.php">NomadPOS</a>
            <ul class="sub-nav">
                <li><a href="#">For whom?</a></li>
                <li><a href="#">Why?</a></li>
            </ul>
        </li>
        <li><a href="#">Pricing</a></li>
        <li><a href="#">Selling with NomadPOS</a></li>
        <li><a href="#">Help</a>
            <ul class="sub-nav">
                <li><a href="#">FAQs</a></li>
                <li><a href="#">Contact us</a></li>
            </ul>
        </li>
    </ul>
</div>