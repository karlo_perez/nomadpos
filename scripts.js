

// CUSTOM FEATURE: MOBILE NAVIGATION COLLAPSE
// APPLIED ON: main navigaton menu
// * this function aids in controlling the layout and behavior of the navigation on mobile-layout
// * the body element's scrolling will also be disabled upon appending of the active-menu class - upon clicking the link tag that has the mobileMenuToggle function
// * when browser window is being resized from mobile to desktop screen, the active-menu class is removed
var toggle = document.querySelector( '.nav-mobile' ),
nav = document.querySelector( '.nav');

function mobileMenuToggle() {
    if ( nav.className === 'nav' ) {
        nav.classList.add( 'active-menu' );
        toggle.classList.add( 'active-toggle' );
    } else {
        nav.classList.remove( 'active-menu' );
        toggle.classList.remove( 'active-toggle' );
    }
}


window.addEventListener( 'resize', resizeWindow );

function resizeWindow() {
    windowSize = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

    if ( windowSize >= 540 && nav.classList.contains( 'active-menu' ) ) {
        nav.classList.remove( 'active-menu' );
        toggle.classList.remove( 'active-toggle' );
    }

    if ( windowSize >= 540 && navUser.classList.contains( 'active-menu-user' ) ) {
        navUser.classList.remove( 'active-menu-user' );
        toggleUser.classList.remove( 'active-toggle-user' );
    }
}